public class ComparadorPerros implements Comparator<Perro> {
	public int compare(Perro perro1, Perro perro2){
		if (perro1.getId() < perro2.getId())
			return -1;
		else 
		if (perro1.getId() == perro2.getId())
			return 0;
		else
			return 1;
	}
}


import java.util.Comparator;

public class ComparadorPerrosRaza implements Comparator<Perro> {

	@Override
	public int compare(Perro perro1, Perro perro2) {
		return perro1.getRaza().compareToIgnoreCase(perro2.getRaza());
	}

}