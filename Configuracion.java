package negocio.servicios;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;


public class Configuracion {
	private static String claseGestorBD = "";
	private static String claseEmisor = "";
	static {
		cargarPropiedades();
	}
	public static IGestorDatos getGestorDatos() {
			try {
				Class<?> classGestorBD = Class.forName(claseGestorBD);
				return (IGestorDatos) classGestorBD.newInstance();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			return null;
	}
	public static IEmisor getEmisor() {
		try {
			Class<?> classEmisor = Class.forName(claseEmisor);
			return (IEmisor) classEmisor.newInstance();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
}

	private static void cargarPropiedades() {
		try(FileReader reader = new FileReader("config.properties")){
			Properties propConfig = new Properties();
			propConfig.load(reader);
			Configuracion.claseGestorBD = propConfig.getProperty("claseGestorPersistencia");
			Configuracion.claseEmisor = propConfig.getProperty("claseEmisor");
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}
}
-------------------------------------- config.properties ----------------------------------
claseGestorPersistencia=qtx.persistencia.memoria.GestorPersistenciaImplMemoria
claseEmisor=util.EmisorArchivoV2


}