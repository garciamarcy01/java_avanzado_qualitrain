import java.util.TreeSet;

public class App {
	public static void main(String[] args) {

		Municipio mun1 = new Municipio("Guerrero", "Buena Vista", 20, 500);
		Municipio mun2 = new Municipio("Perisur", "Volcanes", 12, 456);
		Municipio mun3 = new Municipio("San Nicolas", "Chimalhuacan", 45, 500);

		TreeSet<Municipio> municipios = new TreeSet<>(new CompararMunicipiosPorPoblacion());
		municipios.add(mun1);
		municipios.add(mun2);
		municipios.add(mun3);

		TreeSet<Municipio> municipiosOrdenadosNombre = new TreeSet<>(new CompararMunicipiosNombre());
		municipiosOrdenadosNombre.add(mun1);
		municipiosOrdenadosNombre.add(mun2);
		municipiosOrdenadosNombre.add(mun3);

		// Iterando el TreeSet con construcción "for each": Observe el orden

		System.out.println("************ Poblacion ************");
		for (Municipio unMunicipio : municipios)
			unMunicipio.mostrar();

		System.out.println("************ Nombre ************");
		for (Municipio unMunicipio : municipiosOrdenadosNombre)
			unMunicipio.mostrar();

	}

}

// 1. Genere una clase Estado y otra Municipio, declare atributos de nombre,
// capital, km2, numHabitantes
// para ambos. En el caso del Estado, adicione como atributo el nombre del
// gobernador.

// 2. Por medio de una lista implemente la agregación de un estado por sus
// municipios. Es decir
// implemente el hecho de que un estado está formado por municipios