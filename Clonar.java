public class Municipio {
	
	

	private String nombre;
	private String capital;
	private double km2;
	private int numHabitantes;

	
	
	
	public Municipio(String nombre, String capital, double km2, int numHabitantes) {
		super();
		this.nombre = nombre;
		this.capital = capital;
		this.km2 = km2;
		this.numHabitantes = numHabitantes;
	}
	
	public Municipio clonar() {
		return new Municipio(this.nombre, this.capital, this.km2,
				this.numHabitantes);
	}
}