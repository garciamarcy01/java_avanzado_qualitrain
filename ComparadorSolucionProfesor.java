3A. Implemente los siguientes servicios:
 		-Un servicio que devuelva una lista con los municipios de un estado ordenados por población
 		-Un servicio que devuelva una lista con los municipios ordenados descendentemente por nombre
 
-----------------------------------------------------------------
public class ComparadorMunicipios implements Comparator<Municipio> {

	@Override
	public int compare(Municipio mun1, Municipio mun2) {
		if(mun1.getNumHabitantes() < mun2.getNumHabitantes())
			return -1;
		return 1;
	}

}
-------------------------------------------------------------------------------

public class ComparadorMunicipios2 implements Comparator<Municipio> {

	@Override
	public int compare(Municipio mun1, Municipio mun2) {
		int resulComparacion = mun2.getNombre().compareToIgnoreCase(mun1.getNombre());
		if(resulComparacion != 0)
			return resulComparacion;
		return 1;
	}

}
------------------------------------------------------------------------------------
	public ArrayList<Municipio> getMunicipiosOrdXPoblacion(){
		return this.getMunicipiosOrd(new ComparadorMunicipios());
	}
	public ArrayList<Municipio> getMunicipiosOrdXNombre(){
		return this.getMunicipiosOrd(new ComparadorMunicipios2());
	}

	private ArrayList<Municipio> getMunicipiosOrd(Comparator<Municipio> comparador){
		TreeSet<Municipio> arbolMunicipios = new TreeSet<>(comparador);
		arbolMunicipios.addAll(this.listaMunicipios);
		return new ArrayList<Municipio>(arbolMunicipios);
	}
