//Class Perro

public class Perro{
	private String nombre;
	private String raza;
	private int idPerro;
	private static int contadorPerros = 0;
	
	public Perro(String nombre, String raza){
		this.nombre = nombre;
		this.raza = raza;
		Perro.contadorPerros++;
		this.idPerro=Perro.contadorPerros;
	}
	public boolean esIdenticoA(Perro otroPerro){
		if (this.idPerro != otroPerro.idPerro)
			return false;
		if (!this.nombre.equals(otroPerro.nombre))
			return false;
		if (!this.raza.equals(otroPerro.raza))
			return false;
		return (true);
	}
	public int getId(){
		return this.idPerro;
	}
	public String getNombre(){
		return this.nombre;
	}
	public String toString(){
		return (this.idPerro+".-"+this.nombre+", raza:"+this.raza);
	}
	public void mostrar(){
		System.out.println(this);
	}
	public Perro clonar(){
		Perro clon = new Perro(this.nombre,this.raza);
		clon.idPerro = this.idPerro;
		return clon;
	}
	public boolean equals(Perro otroPerro){
		System.out.println("perro.equals");
		return this.equals(otroPerro);
	}
}


//MAIN
package qtx.test;

import java.util.HashMap;
import java.util.Map.Entry;

public class ProbadorHashMap {
	public static void main(String[] args) {
		HashMap<Integer, Perro> relacionPerros = new HashMap<>();
		Perro fido = new Perro("Fido", "Eléctrico");
		Perro firulais = new Perro("Firulais", "Alazka Malamut");
		Perro cofee = new Perro("Cofee", "Rottweiler");
		Perro solovino = new Perro("Solovino", "Pastor Belga");

		relacionPerros.put(fido.getId(), fido);
		relacionPerros.put(firulais.getId(), firulais);
		relacionPerros.put(cofee.getId(), cofee);
		relacionPerros.put(solovino.getId(), solovino);

		Perro perroBuscado = relacionPerros.get(2);
		System.out.println("Perro con llave 2:" + perroBuscado);

		HashMap<String, Perro> relacionPerros2 = new HashMap<>();
		relacionPerros2.put(fido.getNombre(), fido);
		relacionPerros2.put(firulais.getNombre(), firulais);
		relacionPerros2.put(cofee.getNombre(), cofee);
		relacionPerros2.put(solovino.getNombre(), solovino);

		perroBuscado = relacionPerros2.get("Solovino");
		System.out.println("Perro con llave \"Solovino\":" + perroBuscado);

		// Iterar HashMAp por llave
		System.out.println("\nIteración por llave");
		for (String llaveI : relacionPerros2.keySet()) {
			Perro valorI = relacionPerros2.get(llaveI);
			System.out.println("llave:" + llaveI + ", valor:" + valorI);
		}

		// Iterar HashMAp por valor
		System.out.println("\nIteración por valor");
		for (Perro valorI : relacionPerros2.values()) {
			System.out.println(", valor:" + valorI);
		}

		// Iterar HashMAp por pares llave-valor
		System.out.println("\nIteración por pares llave-valor");
		for (Entry<String, Perro> parI : relacionPerros2.entrySet()) {
			System.out.println("llave:" + parI.getKey() + ", valor:" + parI.getValue());
		}
	}

}
