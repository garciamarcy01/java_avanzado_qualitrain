package qtx.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class CatalogoPeliculas {
	private HashMap< String, ArrayList<String>> catalogo = new HashMap<>();

	public void agregarCategoria(String categoria) {
		catalogo.put(categoria, new ArrayList<>());
	}

	public void agregarPelicula(String nombrePelicula, String categoria) {
		// se supone que el catalogo y la categoria ya existen
		ArrayList<String> vPeliculas = null;

		vPeliculas = this.catalogo.get(categoria);
		vPeliculas.add(nombrePelicula);
		
		catalogo.put(categoria, vPeliculas);
	}

	public Iterator<String> recuperarCategorias() {
		Iterator<String> categorias = null;
		
		categorias = this.catalogo.keySet().iterator();
		
		return categorias;
	}

	public ArrayList<String> recuperarPeliculas(String categoria) {
		ArrayList<String> vPeliculas = null;
		
		vPeliculas = this.catalogo.get(categoria);
		
		return vPeliculas;
	}

}
-----------------------------------------------------------
package qtx.test;

import java.util.ArrayList;
import java.util.Iterator;

public class VideoClub {
	void mostrarPeliculas(CatalogoPeliculas cat) {
		// complete la función para mostrar las películas organizadas por categoría
		Iterator<String> e;
		ArrayList<String> vPeliculas;
		String categoria = null, pelicula = null;

		e = cat.recuperarCategorias();
		while (e.hasNext()) {
			categoria = e.next(); // Recupera la categoria
			System.out.println("\n=========================");
			System.out.println("Categoría: " + categoria);
			System.out.println("=========================");
			vPeliculas = cat.recuperarPeliculas(categoria);
			for(String peliI : vPeliculas)
				System.out.println(peliI);
		}
	}

	public static void main(String args[]) {
		VideoClub miVc = new VideoClub();
		CatalogoPeliculas catPel = new CatalogoPeliculas();
		String categorias[] = { "Terror", "Accion", "Infantil", "Arte" };
		int indice = 0;

		for (indice = 0; indice < 4; indice++)
			catPel.agregarCategoria(categorias[indice]);
		catPel.agregarPelicula("El Exorcista", "Terror");
		catPel.agregarPelicula("El bebe de Rosemary", "Terror");
		catPel.agregarPelicula("Bajo California", "Arte");
		catPel.agregarPelicula("El verano de Kikujiro", "Arte");
		catPel.agregarPelicula("Fuegos Artificiales", "Arte");
		catPel.agregarPelicula("La Cenicienta", "Infantil");
		catPel.agregarPelicula("Fantasia", "Infantil");
		catPel.agregarPelicula("Titan A.E.", "Infantil");
		catPel.agregarPelicula("Mission: Imposible", "Accion");
		catPel.agregarPelicula("La Roca", "Accion");
		miVc.mostrarPeliculas(catPel);
	}

}
