//	Un servicio que permita copiar un estado a otro
	public void copiarEstado(Estado estadoDestino) {
		estadoDestino.setNombre(this.nombre);
		estadoDestino.setCapital(this.capital);
		estadoDestino.setKm2(this.km2);
		estadoDestino.setNombreGobernador(this.nombreGobernador);
		estadoDestino.setNumHabitantes(this.numHabitantes);

		ArrayList<Municipio> municipiosDestino = new ArrayList<>();
		for(Municipio munI : this.listaMunicipios) {
			municipiosDestino.add(munI.clonar());
		}
		estadoDestino.setListaMunicipios(municipiosDestino);
	
	}
