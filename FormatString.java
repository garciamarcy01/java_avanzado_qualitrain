Método en Estado

	public Map<String,Integer> getComboMunicipios(){
		TreeMap<String, Integer> mapCombo = new TreeMap<>();
		for(int i=0; i<this.listaMunicipios.size(); i++) {
			Municipio munI = this.listaMunicipios.get(i);
			String llaveI = String.format("%-27s : %6d", munI.getNombre(),munI.getNumHabitantes() );
			mapCombo.put(llaveI, i);
		}
		return mapCombo;
	}
	
	Código de prueba

	private static void probarComboMunicipios() {
		System.out.println();
		System.out.println("---------------------------------------------");
		System.out.println("Probando combo de municipios");
		System.out.println("---------------------------------------------");
		Municipio municipio1 = new Municipio("Huajuapan de León","Huajuapan de León",361.1f,45321l);
		Municipio municipio2 = new Municipio("Abejones","Abejones",122.5f,1144l);
		Municipio municipio3 = new Municipio("Yanhuitlán","Santo Domingo, Yanhuitlán",227.5f,4882l);
		Municipio municipio4 = new Municipio("Coatecas Altas","Coatecas Altas",125.0f,4882l);
		Municipio municipio5 = new Municipio("Acatlán de Pérez Figueroa","Acatlán de Pérez Figueroa",933.9f,42347l);
		
		ArrayList<Municipio> municipiosOaxaca = new ArrayList<Municipio>();
		municipiosOaxaca.add(municipio1);
		municipiosOaxaca.add(municipio2);
		municipiosOaxaca.add(municipio3);
		municipiosOaxaca.add(municipio4);
		municipiosOaxaca.add(municipio5);
		
		Estado oaxaca = new Estado(municipiosOaxaca);
		oaxaca.setNombreGobernador("El tal Murat");
		oaxaca.setKm2(93757);
		oaxaca.setNombre("Oaxaca");
		oaxaca.setNumHabitantes(3802000l);
		oaxaca.setCapital("Oaxaca de Juárez");
		System.out.println(oaxaca);

		Map<String, Integer> comboMunicipios = oaxaca.getComboMunicipios();
		System.out.println("\nContenido visible combo:");
		System.out.println("=====================================");
		for(String keyI : comboMunicipios.keySet()) {
			System.out.println(keyI);
		}
		System.out.println("=====================================");
		System.out.println("\nContenido combo con llave:");
		System.out.println("==========================================");
		for(String keyI : comboMunicipios.keySet()) {
			System.out.println(keyI + " (" + comboMunicipios.get(keyI) + ")");
		}
		System.out.println("==========================================");
		
	}
